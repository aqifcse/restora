FROM python:3.11.0a2-alpine3.14
MAINTAINER Syed Towfiqur Rahim

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /restora
WORKDIR /restora
COPY ./restora /restora

RUN adduser -D aqifcse
USER aqifcse